Knot DNS
=========

Ansible role to configure Knot and kresd

Requirements
------------

So far this only works with CentOS 8 and RHEL 8, it hasn't yet been tested on EL7 but I imagine it should work.

Role Variables
--------------
```
# defaults file for fox-den.knot

# Defines the default zone name
domain_name: example.com

# Defines the default listening interfaces for Knot and kresd
knot_dns_ip: '{{ ansible_default_ipv4["address"] }}'
knot_resolver_ip: '{{ ansible_default_ipv4["address"] }}'

# Defines the default remote resolvers is kresd is installed and set to resolve remote names
dns_resolvers:
  - 8.8.8.8
  - 8.8.4.4

# Defines whether or not kresd should be installed
knot_recursive: false

# Defines which network should be allowed to update RDNS
knot_subnet: 192.168.1.0/24

# Defines whether or not logging should be enabled
knot_logging: false

# Defines knot logging format. Targets: stdout | stderr | syslog | /path/to/log
knot_log_format: syslog

# Defines knot log level. Levels: critical | error | warning | notice | info | debug
knot_log_level: info
```

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: dns
      roles:
        - role: foxden.knot
          vars:
            domain_name: foxden.cloud
            dns_resolvers:
              - 208.67.222.222


License
-------

BSD

Author Information
------------------

Erin Murphy - https://foxden.cloud
